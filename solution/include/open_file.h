#ifndef READ_WRITE_FROM_FILE_H
#define READ_WRITE_FROM_FILE_H

#include <stdio.h>

int open_status_check(FILE* f);

FILE* open_readable_file(char *path);

FILE* open_writable_file(char *path);

#endif //READ_WRITE_FROM_FILE_H
