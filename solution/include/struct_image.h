#ifndef IMAGE_ROTATION_STRUCT_IMAGE_H
#define IMAGE_ROTATION_STRUCT_IMAGE_H

#include  <stdint.h>

#define STRUCT_HEADER_SIZE sizeof(struct bmp_header)
#define BMP_SIGNATURE 0x4D42
#define BIT_COUNT 24
#define PLANES_COUNT 1
#define HEADER_SIZE 40

#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

struct image {
    uint64_t width, height;
    struct pixel *data;
};

#pragma pack(push, 1)
struct pixel {
    uint8_t b, g, r;
};
#pragma pack(pop)

#endif //IMAGE_ROTATION_STRUCT_IMAGE_H
