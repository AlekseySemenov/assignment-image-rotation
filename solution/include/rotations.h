#ifndef IMAGE_ROTATION_ROTATIONS_H
#define IMAGE_ROTATION_ROTATIONS_H

struct image rotate( struct image const source );

#endif //IMAGE_ROTATION_ROTATIONS_H
