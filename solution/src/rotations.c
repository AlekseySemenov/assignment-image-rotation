#include <malloc.h>

#include "rotations.h"
#include "struct_image.h"

static uint64_t position_in_final_image(uint64_t i, uint64_t j, uint64_t height, uint64_t width){
    return (height - j) * width - 1 - i;
}

static uint64_t position_in_source_image(uint64_t i, uint64_t j, uint64_t width){
    return (i + 1) * width - 1 - j;
}


struct image rotate(struct image const source) {
    struct image img = {.width = source.height, .height = source.width};
    img.data = malloc(img.width * img.height * sizeof(struct pixel));
    for (uint64_t i = 0; i < img.width; i++) {
        for (uint64_t j = 0; j < img.height; j++) {
            img.data[position_in_final_image(i,j, img.height, img.width)] = source.data[position_in_source_image(i,j,source.width)];
        }
    }
    return img;
}
