#include <stdlib.h>

#include "message_printer.h"
#include "open_file.h"
#include "read_write_image_from_file.h"
#include "rotations.h"
#include "struct_image.h"

int main( int argc, char** argv ) {
    if (argc != 3){
        print_error("too few/many arguments");
        return 0;
    }
    struct image img = {0};
    struct image rotated_image = {0};
    FILE *in = open_readable_file(argv[1]);
    if (open_status_check(in) == 0) {
        if (from_bmp(in, &img) != READ_OK) {
            print_error("Read failed\n");
        } else {
            print_message("Read successful\n");
            rotated_image = rotate(img);
            FILE *out = open_writable_file(argv[2]);
            if (open_status_check(out) == 0){
                if (to_bmp(out, &rotated_image) != WRITE_OK) {
                    print_error("Write failed\n");
                }
                print_message("Write successful\n");
                fclose(out);
            }
        }
        fclose(in);
    }
    free(rotated_image.data);
    free(img.data);
    return 0;
}
