#include "read_write_image_from_file.h"
#include "malloc.h"
#include "struct_image.h"

uint32_t padding = 0;

static void bmp_padding_set(uint32_t width) {
    padding = width % 4;
}

static struct bmp_header bmp_header_read(FILE *in) {
    struct bmp_header header = {0};
    fread(&header, STRUCT_HEADER_SIZE, 1, in);
    return header;
}

static enum read_status bmp_header_check(struct bmp_header header) {
    if (header.bfType != 0x4D42)
        return READ_INVALID_SIGNATURE;
    if (header.biBitCount != 24)
        return READ_INVALID_BITS;
    return READ_OK;
}


static enum read_status bmp_image_read(struct image *img, FILE *in) {
    bmp_padding_set(img->width);
    struct pixel *pls = malloc(img->height * (img->width + padding) * sizeof(struct pixel));
    struct pixel *point = pls;
    for (uint64_t i = 0; i < img->height; i++) {
        if (fread(point, sizeof(struct pixel), img->width, in) != img->width || fseek(in, padding, SEEK_CUR) != 0) {
            free(pls);
            return READ_INVALID_BITS;
        }
        point+=img->width;
    }
    img->data = pls;
    return READ_OK;
}

enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header header = bmp_header_read(in);
    enum read_status header_rs = bmp_header_check(header);
    if (header_rs != READ_OK) {
        return header_rs;
    }
    img->width = header.biWidth;
    img->height = header.biHeight;
    enum read_status image_rs = bmp_image_read(img, in);
    if (image_rs != READ_OK) {
        free(img);
        img = NULL;
        return image_rs;
    }
    return READ_OK;
}

static struct bmp_header create_bmp_header(const struct image *img) {
    return (struct bmp_header) {
            .bfType = BMP_SIGNATURE,
            .bfileSize = STRUCT_HEADER_SIZE + img->width*img->height*sizeof(struct pixel) + padding*img->height,
            .bfReserved = 0,
            .bOffBits = STRUCT_HEADER_SIZE,
            .biSize = HEADER_SIZE,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = PLANES_COUNT,
            .biBitCount = BIT_COUNT,
            .biCompression = 0,
            .biSizeImage = img->width*img->height*sizeof(struct pixel) + padding*img->height,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
}

static enum write_status bmp_header_write(struct bmp_header output_header, FILE *out) {
    if (fwrite(&output_header, sizeof(output_header), 1, out) != 1)
        return WRITE_ERROR;
    return WRITE_OK;
}

static enum write_status bmp_image_write(const struct image *img, FILE *out) {
    bmp_padding_set(img->width);
    struct pixel* point = img->data;
    for(uint64_t i = 0; i<img->height;i++){
        if (fwrite(point, sizeof(struct pixel),img->width, out) != img->width || fseek(out, padding, SEEK_CUR) != 0){
            return WRITE_ERROR;
        }
        point+=img->width;
    }
    return WRITE_OK;
}

enum write_status to_bmp(FILE *out, struct image const *img) {
    struct bmp_header output_header = create_bmp_header(img);
    enum write_status header_ws = bmp_header_write(output_header, out);
    if (header_ws!=WRITE_OK){
        return header_ws;
    }
    return bmp_image_write(img, out);
}

