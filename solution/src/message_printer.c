#include <stdio.h>

#include "message_printer.h"

void print_message(char* mes){
    fprintf(stdout,"%s", mes);
}

void print_error(char* mes){
    fprintf(stderr, "%s", mes);
}
