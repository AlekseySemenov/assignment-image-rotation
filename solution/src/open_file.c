#include "open_file.h"
#include "message_printer.h"

int open_status_check(FILE* f){
    if (f == NULL) {
        print_error("Open failed\n");
        return -1;
    }
    return 0;
}

FILE* open_readable_file(char *path) {
    return fopen(path, "rb");
}

FILE* open_writable_file(char *path) {
    return fopen(path, "wb");
}
